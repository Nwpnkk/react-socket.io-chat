import React from 'react';
import './App.css';
import Layout from './views/Main'

function App() {
  return (
      <Layout />
  );
}

export default App;
