const uuid = require('uuid/v4')

const createUser = ({ name = "" } = {}) => ({
    id: uuid(),
    name
})

const createMessage = ({ message = "", sender = "" } = {}) => ({
    id: uuid(),
    message,
    sender
})

const createChatroom = ({ message = [], users = [], name = "Chatroom" } = {}) => ({
    id: uuid(),
    message,
    users,
    typingUsers: []
})

module.exports = {
    createUser,
    createMessage,
    createChatroom
}