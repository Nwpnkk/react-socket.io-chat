const server = require('http').createServer()
const io = require('socket.io')(server)
const Socket = require('./socket')

const port = process.env.PORT || 8080

io.on('connection', Socket)

server.listen(port, () => {

    console.log(`Server listenning at port :: ${port}`)

})

module.exports = io