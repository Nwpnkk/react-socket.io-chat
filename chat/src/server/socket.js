const io = require('./server').io
const config = require('../constant')
const userManager = require('../userManager')

let connected_users = {}
let chatroom = userManager.createChatroom()

module.exports = (socket) => {

    console.log(`Socket connected ID = ${socket.id}`)

    socket.on(config.VERIFY_USER, (name, callback) => {
        if (isUser(connected_users, name)) {
            callback({ isUser: true, user: null })
        } else {
            callback({ isUser: false, user: userManager.createUser({ name }) })
        }
    })

    socket.on(config.USER_CONNECTED, (user) => {
        connected_users = addUser(connected_users, user)
        socket.user = user

        console.log(user)
        // io.emit(config.USER_CONNECTED, connected_users)
    })

    socket.on(config.USER_DISCONNECTED, () => {
        if ("user" in connected_users) {
            connected_users = removeUser(connected_users, socket.user)

            io.emit(config.USER_DISCONNECTED)
        }
    })

}


function isUser(connected_users, user) {
    return user in connected_users
}

function addUser(connected_users, user) {
    let newList = Object.assign({}, connected_users)
    newList[user.name] = user
    return newList
}

function removeUser(connected_users, user) {
    let newList = Object.assign({}, connected_users)
    delete newList[user]
    return newList
}