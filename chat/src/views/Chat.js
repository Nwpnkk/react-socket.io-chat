import React, { Component } from 'react'

export default class Chat extends Component {
    render() {
        const { user } = this.props
        return (
            <div>
                <h3 style={{ textAlign: "center" }} >{user}</h3>
            </div>
        )
    }
}
