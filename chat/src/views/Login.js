import React, { Component } from 'react'
import config from '../constant'


export default class Login extends Component {
    state = {
        user: "",
        err: ""
    }

    handleInputChange = (e) => {
        const target = e.target
        this.setState({ [target.name]: target.value })
    }

    submit = () => {
        const { socket } = this.props
        socket.emit(config.VERIFY_USER, this.state.user, this.verfiyUser)
    }

    verfiyUser = ({ isUser, user }) => {
        if (isUser) {
            this.setState({ err: "This name is already taken" })
        } else {
            this.setState({ err: "" })
            this.props.setUser(user)
        }
    }

    render() {
        return (
            <div>
                <h3 style={{ textAlign: "center", fontWeight: 700 }}>Join us</h3>
                <div style={{ textAlign: 'center' }}>
                    <input style={{ width: 300, height: 25 }} type="text" name="user" value={this.state.user} onChange={this.handleInputChange}
                        onKeyPress={e => e.key === "Enter" && this.submit()} placeholder="enter your name" />
                </div>
            </div>
        )
    }
}
