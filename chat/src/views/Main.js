import React, { Component } from 'react'
import io from 'socket.io-client'
import Login from './Login'
import Chat from './Chat'
import config from '../constant'

const gateWay = "http://localhost:8080"

export default class Main extends Component {

    state = {
        socket: null,
        user: null
    }

    componentWillMount() {
        this.initSocket()
    }

    componentDidMount() {
        if (!process.browser)
            this.logout()
    }


    initSocket = () => {
        const socket = io(gateWay)
        socket.emit("connection", () => console.log(`Socket connected ID = ${socket.id}`))
        this.setState({ socket })
    }

    setUser = (user) => {
        const { socket } = this.state
        socket.emit(config.USER_CONNECTED, user)
        this.setState({ user : user.name})
    }

    logout = () => {
        const { socket } = this.state
        socket.emit(config.USER_DISCONNECTED)
        this.setState({ user: null })
    }
    render() {
        return (
            <div>{
                !this.state.user ? <Login socket={this.state.socket} setUser={this.setUser} /> :
                    <Chat socket={this.state.socket} user={this.state.user} logout={this.logout} />
            }
            </div>
        )
    }
}
